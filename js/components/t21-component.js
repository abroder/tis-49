import React from "react";
import T21 from "../t21.js";

class T21Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {t21: new T21(), program: "", error: ""};
  }

  render() {
    return (
        <div className="t21-cell">
          <textarea value={this.state.program} onChange={this.programUpdated.bind(this)} rows={15} columns={19} />
          <div className="acc">ACC: {this.state.t21._regs.ACC}</div>
          <div className="bak">BAK: {this.state.t21._regs.BAK}</div>
          <div className="last"></div>
          <div className="state"></div>
          <div className="error">{this.state.error}</div>
          <button onClick={this.loadProgram.bind(this)}>Load</button>
          <button onClick={this.stepCell.bind(this)}>Step</button>
        </div>
    );
  }

  programUpdated(e) {
    this.setState({program: e.target.value});
  }

  loadProgram() {
    var t21 = this.state.t21;
    t21.load(this.state.program.split("\n"));
    this.setState({t21, error: ""});
  }

  stepCell() {
    if (this.state.error.length > 0) return;
    
    var t21 = this.state.t21;
    try {
      t21.step();
      this.setState({t21});
    } catch (ex) {
      this.setState({error: "Error: Line " + ex});
    }
  }
}

export default T21Component;