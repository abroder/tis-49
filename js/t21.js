import TNode from "./t-node.js";

class T21 extends TNode {
  constructor() {
    super();

    this._regs = this._resetRegs();
    this._labels = {};
    this._program = undefined;
  }

  load(program) {
    this._resetProgram();
    if (!program) return;

    this._program = [];
    for (var i = 0; i < program.length; ++i) {
      var line = program[i].trim().toUpperCase();

      if (line[line.length - 1] === ':') { // entire line is label
        this._labels[line.substr(0, line.length - 1)] = i;
        this._program.push("NOP");
      } else {
        var colonLocation = line.indexOf(':');
        if (colonLocation >= 0) {
          var label = line.substr(0, colonLocation);
          line = line.substr(colonLocation + 1).trim();
          this._labels[label] = i;
        }
        this._program.push(line);
      }
    }
  }

  step() {
    if (!this._program || this._program.length == 0) return;

    var line = this._program[this._regs.PC++];
    line = line.split(" "); // TODO: more generous with formatting (extra whitespace)
    this[line[0]].apply(this, line);

    if (this._regs.PC >= this._program.length) this._regs.PC = 0;
  }

  NOP() {
    this._checkArguments(arguments, 1);

    // this.ADD(this._regs.NIL);
  }

  MOV() {
    this._checkArguments(arguments, 3);
    var src = this._checkSrc(arguments);
    var dest = this._checkDest(arguments);

    if (this._ports[dest] === null || src === null) {
      this._regs.PC--;
    } else if (this._ports[dest] !== undefined) {
      this._ports[dest] = src;
    } else {
      this._regs[dest] = src;
    }
  }

  SWP() {
    this._checkArguments(arguments, 1);

    var temp = this._regs.BAK;
    this._regs.BAK = this._regs.ACC;
    this._regs.ACC = temp;
  }

  SAV() {
    this._checkArguments(arguments, 1);

    this._regs.BAK = this._regs.ACC;
  }

  ADD() {
    this._checkArguments(arguments, 2);
    var src = this._checkSrc(arguments);
    if (src === null) this._regs.PC--;
    else {
      this._regs.ACC = this._clamp(this._regs.ACC + src, -999, 999);
    }
  }

  SUB() {
    this._checkArguments(arguments, 2);
    var src = this._checkSrc(arguments);
    if (src === null) this._regs.PC--;
    else {
      this._regs.ACC = this._clamp(this._regs.ACC - src, -999, 999);
    }
  }

  NEG() {
    this._checkArguments(arguments, 1);

    this._regs.ACC *= -1;
  }

  JMP() {
    this._checkArguments(arguments, 2);
    var label = this._checkLabel(arguments);

    this._regs.PC = this._labels[label];
  }

  JEZ() {
    this._checkArguments(arguments, 2);
    var label = this._checkLabel(arguments);

    if (this._regs.ACC === 0) {
      this._regs.PC = this._labels[label];
    }
  }

  JNZ() {
    this._checkArguments(arguments, 2);
    var label = this._checkLabel(arguments);

    if (this._regs.ACC !== 0) {
      this._regs.PC = this._labels[label];
    }
  }

  JGZ() {
    this._checkArguments(arguments, 2);
    var label = this._checkLabel(arguments);

    if (this._regs.ACC > 0) {
      this._regs.PC = this._labels[label];
    }
  }

  JLZ() {
    this._checkArguments(arguments, 2);
    var label = this._checkLabel(arguments);

    if (this._regs.ACC < 0) {
      this._regs.PC = this._labels[label];
    }
  }

  JRO() {
    this._checkArguments(arguments, 2);
    var src = this._checkSrc(arguments);
    if (src === null) this._regs.PC--;
    else {
      this._regs.PC = this._clamp(this._regs.PC + src, 0, this._program.length - 1);
      this._regs.PC--; // to account for PC increment after instruction finishes
    }
  }

  _checkArguments(args, count) {
    if (args.length != count) throw this._regs.PC;
  }

  _checkSrc(args) {
    var src = args[1];

    if (!isNaN(src)) return +src;
    if (this._ports[src] !== undefined) return this._ports[src];
    if (this._regs[src] !== undefined && src !== 'BAK') return this._regs[src];

    throw this._regs.PC;
  }

  _checkDest(args) {
    var dest = args[2];

    if (dest === 'BAK'
        || (this._ports[dest] === undefined
            && this._regs[dest] === undefined)) throw this._regs.PC;

    return dest;
  }

  _checkLabel(args) {
    var label = args[1];
    if (this._labels[label] === undefined) throw this._regs.PC;
    return label;
  }

  _clamp(value, min, max) {
    if (value > max) return max;
    if (value < min) return min;
    return value;
  }

  _resetProgram() {
    this._program = undefined;
    this._labels = {};
    this._regs = this._resetRegs();

    // TODO: figure out how to reset ports
  }

  _resetRegs() {
    return { ACC: 0, BAK: 0, NIL: 0, PC: 0 };
  }
};

export default T21;